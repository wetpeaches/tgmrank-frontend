import React from 'react';

import { Helmet } from 'react-helmet-async';
import errorHeader from '../../images/error-header.svg';
import { Trans } from '@lingui/macro';
import { Link, navigate } from '@reach/router';
import Layout from '../components/layout';

export function notFoundRedirect(
  message = 'We could not find the page you were looking for.',
) {
  navigate('/not-found', {
    state: {
      message: message,
    },
    replace: true,
  });
}

export default function NotFoundPage(props) {
  const message = props.location?.state?.message ?? 'There’s nothing here!';
  return (
    <Layout>
      <Helmet>
        <title>Page not found</title>
      </Helmet>
      <div className="error">
        <img className="error__img" src={errorHeader} alt="" />
        <h2>404: {message}</h2>
        <p>
          <Trans>
            Try your luck on the <Link to="/">Homepage</Link>
          </Trans>
        </p>
      </div>
    </Layout>
  );
}
