import objstr from 'obj-str';
import React from 'react';
import {
  Link as IconLink,
  Twitter as IconTwitter,
  Video as IconVideo,
} from 'react-feather';
import twitchLogo from '../../../images/proof-icons/twitch.svg';
import './proof-thumbnails.css';
import { ProofType } from '../../tgmrank-api';

// import youtubeLogo from '../../../images/proof-icons/youtube.svg';

function getYoutubeId(url) {
  return url.match(
    /^.*(?:(?:youtu\.be\/|v\/|vi\/|u\/\w\/|embed\/)|(?:(?:watch)?\?v(?:i)?=|&v(?:i)?=))([^#&?]*).*/,
  )?.[1];
}

function renderIcon(link) {
  const url = new URL(link);
  if (url.hostname.endsWith('twitch.tv')) {
    return <img src={twitchLogo} alt="" />;
  } else if (url.hostname.includes('youtu')) {
    return (
      <img
        src={`https://i.ytimg.com/vi/${getYoutubeId(link)}/default.jpg`}
        alt=""
      />
    );
  } else {
    return <IconVideo />;
  }
}

export function ProofThumbnails({ displayedProof, proof, onClick }) {
  return (
    proof?.length > 1 && (
      <div className="score-details__proofs">
        <div className="proof-thumbnails">
          {proof.map((proof, index) => {
            const url = new URL(proof.link);
            let className;
            if (url.hostname.endsWith('twitch.tv')) {
              className = 'twitch';
            }
            return (
              <button
                key={index}
                onClick={() => onClick(index)}
                className={objstr({
                  'proof-thumbnails__item': true,
                  'proof-thumbnails__item--active': displayedProof === index,
                  'proof-thumbnails__item--twitch': className === 'twitch',
                })}
              >
                {proof.type === ProofType.Image ? (
                  <img src={proof.link} />
                ) : proof.type === ProofType.Video ? (
                  renderIcon(proof.link)
                ) : url.hostname.includes('twitter') ? (
                  <IconTwitter />
                ) : (
                  <IconLink />
                )}
              </button>
            );
          })}
        </div>
      </div>
    )
  );
}
