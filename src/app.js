import React from 'react';
import { Router } from '@reach/router';
import Alert from '@reach/alert';
import { Helmet } from 'react-helmet-async';
import 'cross-fetch/polyfill';
import objstr from 'obj-str';
import { DefaultToast, ToastProvider } from 'react-toast-notifications';
import { SWRConfig } from 'swr';
import { DataProvider } from './contexts/data';
import { GamesProvider } from './hooks/use-games';

import NotFoundPage from './pages/404';
import PrivacyPolicyPage from './pages/about/privacy-policy';
import ProofPolicyPage from './pages/about/proof-policy';
import AdminPage from './pages/admin';
import AboutPage from './pages/about';
import ChallengesPage from './pages/challenges';
import HomePage from './pages/home';
import PlayerPage from './pages/player';
import LeaderboardPage from './pages/leaderboard';
import { FightcadeLeaderboardPage } from './pages/leaderboard';
import LoginPage, {
  ForgotPasswordPage,
  ResetPasswordPage,
} from './pages/login';
import LogoutPage from './pages/logout';
import SettingsPage from './pages/settings';
import AddScorePage, { UpdateScorePage } from './pages/add-score';
import ScorePage from './pages/score';
import DeathSeriesCalculator from './pages/death-calculator';
import { StatsPage } from './pages/stats';
import PlayerSummaryPage from './pages/player/summary';

import VerificationQueuePage from './pages/admin/verification-queue';

import { UserProvider } from './contexts/user';
import { LanguageProvider } from './contexts/language';
import { UserLocationsProvider } from './contexts/locations';
import { ThemeConsumer, ThemeProvider } from './contexts/theme';

import { LeaderboardFilterProvider } from './components/leaderboard-header/filter';

import { fetcher } from './api';

import './styles.css';
import './buttons.css';

const CustomToast = ({ children, ...props }) => (
  <Alert>
    <DefaultToast {...props}>{children}</DefaultToast>
  </Alert>
);

const routes = [
  <NotFoundPage key="not-found-default" default />,
  <NotFoundPage key="/not-found" path="/not-found" />,
  <HomePage key="/" path="/" />,
  <AboutPage key="/about" path="/about" />,
  <PrivacyPolicyPage key="/about/privacy" path="/about/privacy" />,
  <ProofPolicyPage key="/about/proof" path="/about/proof" />,
  <AdminPage key="/admin" path="/admin" />,
  <LoginPage key="/login" path="/login" />,
  <LoginPage key="/sign-up" path="/sign-up" />,
  <LogoutPage key="/logout" path="/logout" />,
  <SettingsPage key="/settings" path="/settings" />,
  <SettingsPage key="/settings/profile" path="/settings/profile" />,
  <SettingsPage key="/settings/security" path="/settings/security" />,
  <ForgotPasswordPage key="/forgot-password" path="/forgot-password" />,
  <ResetPasswordPage
    key="/reset-password/:playerName/:resetKey"
    path="/reset-password/:playerName/:resetKey"
  />,
  <PlayerPage key="/player/:playerName" path="/player/:playerName" />,
  <PlayerSummaryPage
    key="/player/:playerName/summary"
    path="/player/:playerName/summary"
  />,
  <PlayerPage key="/player/:playerName/:tab" path="/player/:playerName/:tab" />,
  <DeathSeriesCalculator key="/tools/death" path="/tools/death" />,
  <LeaderboardPage key="/:game" path="/:game" />,
  <LeaderboardPage key="/:game/:mode" path="/:game/:leaderboard" />,
  <StatsPage key="/stats" path="/stats" />,
  <ScorePage key="/score/:scoreId" path="/score/:scoreId" />,
  <AddScorePage key="/score/submit" path="/score/submit" />,
  <UpdateScorePage key="/score/:scoreId/edit" path="/score/:scoreId/edit" />,
  <VerificationQueuePage
    key="/admin/verification-queue"
    path="/admin/verification-queue"
  />,
  <ChallengesPage key="/event/archive" path="/event/archive" />,
  <FightcadeLeaderboardPage key="/fightcade" path="/fightcade" />,
];

export default function App() {
  return (
    <SWRConfig
      value={{
        fetcher: fetcher,
      }}
    >
      <GamesProvider>
        <UserProvider>
          <ToastProvider components={{ Toast: CustomToast }}>
            <LanguageProvider>
              <UserLocationsProvider>
                <LeaderboardFilterProvider>
                  <ThemeProvider>
                    <ThemeConsumer>
                      {({ darkTheme }) => (
                        <Helmet titleTemplate="%s - theabsolute.plus">
                          <body
                            className={objstr({ 'dark-theme': darkTheme })}
                          />
                        </Helmet>
                      )}
                    </ThemeConsumer>
                    <Router>{routes}</Router>
                  </ThemeProvider>
                </LeaderboardFilterProvider>
              </UserLocationsProvider>
            </LanguageProvider>
          </ToastProvider>
        </UserProvider>
      </GamesProvider>
    </SWRConfig>
  );
}
