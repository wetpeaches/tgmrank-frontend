import React from 'react';

export default function LanguageSwitcherIcon(props) {
  return (
    <svg viewBox="0 0 22 22" {...props}>
      <g fill="none" strokeWidth="2">
        <path d="M0 4h16M8 4V1M12 5c0 2-3.3 6-10 12M5 7c0 2 5 7 7 8" />
        <path strokeLinejoin="bevel" d="M12 21v-.4L16.5 9 21 20.6v.4" />
        <path d="M14 17h5" />
      </g>
    </svg>
  );
}
