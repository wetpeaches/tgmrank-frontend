import React from 'react';

export default function Chevron(props) {
  return (
    <svg viewBox="0 0 12 12" {...props}>
      <path
        stroke="#8e8e8e"
        strokeWidth="2"
        d="M1 4l5 5 5-5"
        fill="none"
        strokeLinecap="round"
      />
    </svg>
  );
}
