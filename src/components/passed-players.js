import React, { useState } from 'react';
import Link from './link';
import { useLingui } from '@lingui/react';
import { Trans, t } from '@lingui/macro';
import Dialog from './dialog';
import ModeName, { GameLink, ModeLink } from './mode-name';
import { useGames } from '../hooks/use-games';

// TODO Dedupe
function insertIf(condition, ...elements) {
  return condition ? elements : [];
}

function PlayersList({ players }) {
  const { i18n } = useLingui();
  const [showDialog, setState] = useState(false);
  return (
    <>
      {players.length <= 5 ? (
        <>
          {players.map((player, i) => (
            <React.Fragment key={player.playerId}>
              {i > 0 &&
                (i !== players.length - 1
                  ? i18n._(t`comma`, `, `)
                  : players.length > 1 &&
                    i === players.length - 1 &&
                    i18n._(t`comma.and`, `, and `))}
              <Link to={`/player/${player.playerName}`}>
                {player.playerName}
              </Link>
            </React.Fragment>
          ))}
        </>
      ) : (
        <>
          {players.slice(0, 3).map((player, i) => (
            <React.Fragment key={player.playerId}>
              {i > 0 && i18n._(t`comma`, `, `)}
              <Link to={`/player/${player.playerName}`}>
                {player.playerName}
              </Link>
            </React.Fragment>
          ))}
          {i18n._(t`comma.and`, `, and `)}
          <Trans>
            <button
              className="activity__more"
              onClick={event => {
                event.stopPropagation();
                setState(true);
              }}
            >
              {players.length - 3} more
            </button>{' '}
            players
          </Trans>
          <Dialog
            aria-label="Close passed-players dialog"
            isOpen={showDialog}
            close={() => setState(false)}
          >
            <ul className="list-bare">
              {players.slice(3).map(({ playerName, playerId }) => (
                <li key={playerId}>
                  <Link to={`/player/${playerName}`}>{playerName}</Link>
                </li>
              ))}
            </ul>
          </Dialog>
        </>
      )}
    </>
  );
}

export default function PassedPlayers({ delta, game, mode }) {
  const { games } = useGames();
  const categories = [
    ...insertIf(delta.overall.playersOffset.length > 0, {
      title: <GameLink game={games?.overall} />,
      players: delta.overall.playersOffset,
    }),
    ...insertIf(delta.game.playersOffset.length > 0, {
      title: <GameLink game={game} />,
      players: delta.game.playersOffset,
    }),
    ...insertIf(delta.mode.playersOffset.length > 0, {
      title: (
        <ModeLink game={game} mode={mode}>
          <ModeName includeBadge={false} includeSuffix={false} {...mode} />
        </ModeLink>
      ),
      players: delta.mode.playersOffset,
    }),
  ];

  return categories.map(
    ({ title, players }, index) =>
      players.length > 0 && (
        <div key={index} className="activity-item__passed">
          <Trans>
            Passed <PlayersList players={players} /> in {title} rankings.
          </Trans>
        </div>
      ),
  );
}
