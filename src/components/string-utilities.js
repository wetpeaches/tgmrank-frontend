export function toTitleCase(s) {
  return s.replace(
    /\w\S*/g,
    match => match.charAt(0).toUpperCase() + match.substr(1).toLowerCase(),
  );
}

export function asCsv(s) {
  return (
    s
      ?.split(',')
      .map(l => l.trim())
      .filter(l => l) ?? []
  );
}

export function asDateTime(s) {
  if (s == null) {
    return null;
  }

  try {
    return new Date(s).toISOString();
  } catch (e) {
    return null;
  }
}

export function isString(s) {
  return typeof s === 'string';
}

function interleave(array, element) {
  return array.flatMap((value, index, array) =>
    array.length - 1 !== index ? [value, element] : value,
  );
}

export function replaceMap(input, separator, replacement) {
  let elements;
  if (Array.isArray(input)) {
    elements = input;
  } else if (isString(input)) {
    elements = [input];
  } else {
    throw new Error('Expected an Array or String');
  }

  return elements
    .flatMap(e =>
      isString(e) ? interleave(e.split(separator), replacement) : e,
    )
    .filter(e => e);
}

export function matchMap(input, regex, matchFn) {
  let elements;
  if (Array.isArray(input)) {
    elements = input;
  } else if (isString(input)) {
    elements = [input];
  } else {
    throw new Error('Expected an Array or String');
  }

  return elements.flatMap(e => {
    if (!isString(e)) {
      return e;
    }

    const matches = e.match(regex);
    if (matches != null) {
      return matchFn(matches);
    }

    return e;
  });
}
