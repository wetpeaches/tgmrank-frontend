import React from 'react';
import Tooltip from '@reach/tooltip';

import '@reach/tooltip/styles.css';

import iconSmallSilverShield from '../../images/decoration/small-silver-shield.png';
import iconSmallGoldShield from '../../images/decoration/small-gold-shield.png';
import iconSmallSilverMedal from '../../images/decoration/small-silver-medal.png';
import iconSmallGoldMedal from '../../images/decoration/small-gold-medal.png';
import iconLargeSilverShield from '../../images/decoration/large-silver-shield.png';
import iconLargeGoldShield from '../../images/decoration/large-gold-shield.png';
import iconLargeSilverMedal from '../../images/decoration/large-silver-medal.png';
import iconLargeGoldMedal from '../../images/decoration/large-gold-medal.png';
import iconSilverJewel from '../../images/decoration/silver-jewel.png';
import iconGoldJewel from '../../images/decoration/gold-jewel.png';
import iconBigDecoration from '../../images/decoration/big.png';
import iconHugeDecoration from '../../images/decoration/huge.png';
import LeaderboardRank from './leaderboard/rank';
import useSWR from 'swr';
import TgmRank from '../tgmrank-api';
import { useGames } from '../hooks/use-games';

function determineDecoration(rank) {
  if (rank === 1) {
    return iconGoldJewel;
  } else if (rank <= 3) {
    return iconSilverJewel;
  } else if (rank <= 5) {
    return iconLargeGoldMedal;
  } else if (rank <= 10) {
    return iconLargeSilverMedal;
  } else if (rank <= 20) {
    return iconLargeGoldShield;
  } else if (rank <= 30) {
    return iconLargeSilverShield;
  } else if (rank <= 50) {
    return iconSmallGoldMedal;
  } else if (rank <= 75) {
    return iconSmallSilverMedal;
  } else if (rank <= 100) {
    return iconSmallGoldShield;
  } else if (rank <= 150) {
    return iconSmallSilverShield;
  }
  return null;
}

function groupPlacements(scores) {
  let decorationMap = new Map([
    [iconGoldJewel, []],
    [iconSilverJewel, []],
    [iconLargeGoldMedal, []],
    [iconLargeSilverMedal, []],
    [iconLargeGoldShield, []],
    [iconLargeSilverShield, []],
    [iconSmallGoldMedal, []],
    [iconSmallSilverMedal, []],
    [iconSmallGoldShield, []],
    [iconSmallSilverShield, []],
  ]);

  return scores?.reduce((acc, score) => {
    const decoration = determineDecoration(score.rank);
    if (decoration != null) {
      acc.get(decoration).push(score);
    }
    return acc;
  }, decorationMap);
}

export function SingleDecoration({ score }) {
  return determineDecoration(score?.rank);
}

export default function GroupDecoration({ playerId }) {
  const { data: scores } = useSWR(
    playerId != null ? TgmRank.getPlayerScoresUrl(playerId) : null,
  );

  const { games } = useGames();

  if (games == null || scores == null) {
    return null;
  }

  const rankedScores = scores.filter(
    s => s.rank != null && games.lookup?.(s.gameId, s.modeId).mode?.isRanked,
  );
  const decoration = [...groupPlacements(rankedScores).entries()];
  const decorationRows = [
    decoration.slice(0, 2).filter(([, scores]) => scores.length > 0),
    decoration.slice(2, 6).filter(([, scores]) => scores.length > 0),
    decoration.slice(6).filter(([, scores]) => scores.length > 0),
  ].filter(row => row?.length > 0);

  return (
    <div className="Decoration">
      {decorationRows.map((row, rowIndex) => (
        <React.Fragment key={rowIndex}>
          {row.map(([icon, scores]) =>
            scores.map(score => {
              const { game, mode } = games.lookup?.(score.gameId, score.modeId);
              return (
                <Tooltip
                  key={score.scoreId}
                  label={
                    <>
                      <LeaderboardRank rank={score.rank} /> in {game.shortName}{' '}
                      {mode.modeName}
                    </>
                  }
                >
                  <img style={{ width: 'auto' }} src={icon} />
                </Tooltip>
              );
            }),
          )}
          <br />
        </React.Fragment>
      ))}
    </div>
  );
}
