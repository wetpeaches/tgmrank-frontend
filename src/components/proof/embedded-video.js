import React from 'react';
import { OtherProof } from './other-proof';

export function getYoutubeEmbedUrl(url) {
  const getVideoId = url => {
    const regex = /(?:https?:\/\/)?(?:www\.)?(?:youtube\.com\/(?:embed\/|v\/|live\/|watch\?v=|watch\?.+&v=)|youtu\.be\/)((\w|-){11})/;
    const match = url.match(regex);
    return match ? match[1] : null;
  };

  const videoId = getVideoId(url);

  if (videoId == null) {
    return null;
  }

  const embedUrl = `https://www.youtube.com/embed/${videoId}`;

  return embedUrl;
}

export function getTwitchEmbedUrl(url) {
  const matches = url.match(/twitch.tv\/(\w+)\/(?:[vc]\/)?(\d+)(\?.+)?/);
  if (matches == null) {
    return null;
  }

  let username = matches[1];
  if (username == null || username === 'videos') {
    username = null;
  }
  let videoId = matches[2];
  let flags = matches[3] ? `&${matches[3]}` : '';

  return `https://player.twitch.tv/?autoplay=false&video=v${videoId}${flags}&parent=localhost&parent=theabsolute.plus`;
}

export default function EmbeddedVideo({ link }) {
  let src;
  const { hostname } = link;

  // TODO: Make this more resilient
  if (hostname.endsWith('twitch.tv')) {
    src = getTwitchEmbedUrl(link.href);
  } else if (hostname.includes('youtu')) {
    src = getYoutubeEmbedUrl(link.href);
  } else if (hostname.includes('streamable')) {
    const shortcode = link.href.match(/streamable.com\/([^/]+)/)?.[1];
    if (shortcode != null) {
      src = `https://streamable.com/o/${shortcode}`;
    }
  } else {
    return (
      <div className="score-details__proof-video">
        <video
          style={{ width: '100%', height: '100%' }}
          controls
          autoPlay=""
          loop=""
          muted=""
        >
          <source src={link.toString()} />
        </video>
      </div>
    );
  }

  if (src == null) {
    return <OtherProof url={link} />;
  }

  return (
    <div className="score-details__proof-video">
      <iframe
        src={src}
        frameBorder="0"
        allow="autoplay; encrypted-media"
        allowFullScreen
        title="video"
      />
    </div>
  );
}
