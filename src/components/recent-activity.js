import { navigate } from '@reach/router';
import React, { useState } from 'react';
import TgmRank, { ScoreStatus } from '../tgmrank-api';
import useSWR from 'swr';
import Wrapper from './cell-wrapper';
import ActivityCell from './cell/activity';
import Spinner from './spinner';

function RecentActivityPage({
  playerName,
  notableScoresOnly,
  modeIds,
  page = 1,
}) {
  const isPlayerActivity = playerName != null;
  const { data, error } = useSWR(
    TgmRank.getRecentActivityUrl({
      playerName,
      page: page,
      pageSize: 10,
      modeIds,
      ...(notableScoresOnly && {
        rankThreshold: 10,
      }),
      scoreStatuses: [
        ScoreStatus.Legacy,
        ScoreStatus.Pending,
        ScoreStatus.Accepted,
        ScoreStatus.Verified,
      ],
    }).toString(),
  );

  if (error) {
    return `Error! ${error}`;
  }

  function onClick(event, recentActivityItem) {
    const link = `/score/${recentActivityItem.score.scoreId}`;
    if (event.ctrlKey || event.metaKey) {
      window.open(link);
    } else {
      navigate(link);
    }
  }

  return (
    data?.map((item, index) => (
      <Wrapper key={index} onClick={event => onClick(event, item)}>
        <ActivityCell
          showProfile={!isPlayerActivity}
          showImprovements={true}
          showDelta={true}
          item={item}
        />
      </Wrapper>
    )) ?? <Spinner />
  );
}

export default function RecentActivity({
  playerName,
  modeIds,
  notableScoresOnly = false,
}) {
  const [pageCount, setPageCount] = useState(1);

  const pages = [];
  for (let i = 0; i < pageCount; i++) {
    pages.push(
      <RecentActivityPage
        key={i}
        page={i + 1}
        playerName={playerName}
        notableScoresOnly={notableScoresOnly}
        modeIds={modeIds}
      />,
    );
  }

  return <div className="activity">{pages}</div>;
}
