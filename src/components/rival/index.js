import useSWR, { mutate } from 'swr';
import React from 'react';
import { Trans } from '@lingui/macro';
import Link from '../link';
import LeaderboardRank from '../leaderboard/rank';
import ModeName from '../mode-name';
import { useUser } from '../../contexts/user';
import { useToastManager } from '../../hooks/use-toast-manager';
import TgmRank from '../../tgmrank-api';
import './styles.css';
import Tooltip from '@reach/tooltip';
import {
  leaderboardPath,
  RankedModeKeys,
  useGames,
} from '../../hooks/use-games';
import { AggregateLeaderboardLabel } from '../leaderboard/label';
import Tag, { TagGroup } from '../tag';

export const MAX_RIVALS = 5;

export function RivalControl({ rivalPlayerId }) {
  const toastManager = useToastManager();
  const { user, isLoggedIn } = useUser();
  const shouldShowButtons = isLoggedIn && rivalPlayerId !== user.userId;

  const { data: rivalData } = useSWR(
    shouldShowButtons ? TgmRank.getRivalsUrl(user.userId) : null,
  );

  if (!shouldShowButtons || rivalData == null) {
    return '';
  }

  const addRival = async event => {
    event.preventDefault();
    try {
      const newRival = await TgmRank.addRival(user.userId, rivalPlayerId);
      await mutate(TgmRank.getRivalsUrl(user.userId), [...rivalData, newRival]);
      toastManager.addSuccess(
        `Added ${newRival.playerName} to your rivals list.`,
      );
    } catch (e) {
      toastManager.addError(e.error);
    }
  };

  const removeRival = async event => {
    event.preventDefault();
    try {
      await TgmRank.removeRival(user.userId, rivalPlayerId);
      const removedRival = rivalData.find(r => r.playerId === rivalPlayerId);
      await mutate(
        TgmRank.getRivalsUrl(user.userId),
        rivalData.filter(r => r.playerId !== rivalPlayerId),
      );
      toastManager.addSuccess(
        `Removed ${removedRival.playerName} from your rivals list.`,
      );
    } catch (e) {
      toastManager.addError(e.error);
    }
  };

  if (rivalData?.find(r => r.playerId === rivalPlayerId)) {
    return (
      <button onClick={removeRival} className="button">
        <Trans>Remove Rival</Trans>
      </button>
    );
  } else if (rivalData?.length >= MAX_RIVALS) {
    return (
      <button className="button" disabled>
        <Trans>No Rival Slots Available</Trans>
      </button>
    );
  }
  return (
    <button onClick={addRival} className="button">
      <Trans>Add Rival</Trans>
    </button>
  );
}

function RankCell({ score }) {
  if (score?.scoreId == null) {
    return <LeaderboardRank rank={score?.rank} />;
  }
  return (
    <Link to={`/score/${score?.scoreId}`}>
      <LeaderboardRank rank={score?.rank} />
    </Link>
  );
}

function ReverseRivals({ playerId }) {
  let { data: rivaledBy } = useSWR(
    playerId != null ? TgmRank.getRivaledByUrl(playerId) : null,
  );

  if (rivaledBy == null || rivaledBy.length === 0) {
    return null;
  }

  return (
    <TagGroup>
      <Tooltip label="These are the players that have you as a rival">
        <Trans>
          <span>Reverse Rivals:</span>
        </Trans>
      </Tooltip>

      {rivaledBy.map(r => (
        <Tag key={r.playerId}>
          <Link to={`/player/${r.playerName}/rivals`}>{r.playerName}</Link>
        </Tag>
      ))}
    </TagGroup>
  );
}

export function RivalTable({ playerData, rivals }) {
  let { games } = useGames();

  let tablePlayerIds = [playerData.playerId].concat(
    rivals?.map(r => r.playerId),
  );

  let { data: pbData } = useSWR(
    rivals != null ? TgmRank.getMultiplePlayerPbsUrl(tablePlayerIds) : null,
  );

  let { data: overallData } = useSWR(
    rivals != null
      ? TgmRank.getAggregatedRankingUrl(null, {
          modeIds: games?.overall.rankedModes.main,
          playerIds: tablePlayerIds,
        })
      : null,
  );

  let { data: extendedOverallData } = useSWR(
    rivals != null
      ? TgmRank.getAggregatedRankingUrl(null, {
          modeIds: games?.overall.rankedModes.extended,
          playerIds: tablePlayerIds,
        })
      : null,
  );

  // TODO Handle error message
  if (overallData?.error) return null;
  if (extendedOverallData?.error) return null;

  const players = [playerData].concat(rivals ?? []);

  return (
    <div className="rival-table">
      <ReverseRivals playerId={playerData?.playerId} />

      <table>
        {games?.map(g => (
          <React.Fragment key={g.gameId}>
            <thead>
              <tr>
                <th>
                  <Link to={leaderboardPath({ game: g })}>{g.shortName}</Link>
                </th>
                {players?.map(p => (
                  <th key={p.playerId}>
                    <Link to={`/player/${p.playerName}`}>{p.playerName}</Link>
                  </th>
                ))}
              </tr>
            </thead>
            <tbody>
              <tr>
                <td>
                  <Link
                    to={leaderboardPath({
                      game: g,
                      rankedModeKey: RankedModeKeys.main,
                    })}
                  >
                    <AggregateLeaderboardLabel
                      game={g}
                      rankedModeKey={RankedModeKeys.main}
                    />
                  </Link>
                </td>
                {players?.map(p => (
                  <td key={p?.playerId}>
                    <Link
                      to={leaderboardPath({
                        game: g,
                        rankedModeKey: RankedModeKeys.main,
                      })}
                    >
                      <RankCell
                        score={overallData?.find(
                          o =>
                            o.gameId === g.gameId &&
                            o.player.playerId === p?.playerId,
                        )}
                      />
                    </Link>
                  </td>
                ))}
              </tr>
              <tr>
                <td>
                  <Link
                    to={leaderboardPath({
                      game: g,
                      rankedModeKey: RankedModeKeys.extended,
                    })}
                  >
                    <AggregateLeaderboardLabel
                      game={g}
                      rankedModeKey={RankedModeKeys.extended}
                    />
                  </Link>
                </td>
                {players?.map(p => (
                  <td key={p?.playerId}>
                    <Link
                      to={leaderboardPath({
                        game: g,
                        rankedModeKey: RankedModeKeys.extended,
                      })}
                    >
                      <RankCell
                        score={extendedOverallData?.find(
                          o =>
                            o.gameId === g.gameId &&
                            o.player.playerId === p?.playerId,
                        )}
                      />
                    </Link>
                  </td>
                ))}
              </tr>
              {g?.modes
                ?.filter(m => m.isActive())
                .map(m => (
                  <tr key={m.modeId}>
                    <td>
                      <Link to={leaderboardPath({ game: g, mode: m })}>
                        <ModeName {...m} />
                      </Link>
                    </td>
                    {players?.map(p => (
                      <td key={p.playerId}>
                        <RankCell
                          score={pbData?.find(
                            d =>
                              d.modeId === m.modeId &&
                              d.player.playerId === p?.playerId,
                          )}
                        />
                      </td>
                    ))}
                  </tr>
                ))}
            </tbody>
          </React.Fragment>
        ))}
      </table>
    </div>
  );
}
