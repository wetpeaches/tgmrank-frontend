import { matchMap, replaceMap } from './string-utilities';

describe('string-utilities', () => {
  describe('replaceMap', () => {
    it('should throw an error when input is unexpected', () => {
      const invalidInputSamples = [undefined, null, {}, 10];

      for (const invalidInput of invalidInputSamples) {
        expect(() => replaceMap(invalidInput)).toThrowError(
          'Expected an Array or String',
        );
      }
    });

    it('should make string replacements in string', () => {
      const input = 'abc 123';
      const output = replaceMap(input, /123/, 'xyz');

      expect(output).toStrictEqual(['abc ', 'xyz']);
    });

    it('should make string replacements in array of strings', () => {
      const input = ['abc', 'xyz'];
      const output = replaceMap(input, /^xy|bc$/, '12');

      expect(output).toStrictEqual(['a', '12', '12', 'z']);
    });

    it('should preserve non-string elements in array input', () => {
      const input = [12, 'hello', {}];
      const output = replaceMap(input, 'hello', 'world');

      expect(output).toStrictEqual([12, 'world', {}]);
    });
  });

  describe('matchMap', () => {
    it('should throw an error when input is unexpected', () => {
      const invalidInputSamples = [undefined, null, {}, 10];

      for (const invalidInput of invalidInputSamples) {
        expect(() => matchMap(invalidInput)).toThrowError(
          'Expected an Array or String',
        );
      }
    });

    it('should not map non-strings', () => {
      const mapFn = jest.fn();

      const input = [12, {}];
      const output = matchMap(input, /.*/, mapFn);

      expect(output).toStrictEqual(input);
      expect(mapFn).not.toHaveBeenCalled();
    });

    it('should map regex matches', () => {
      const input = ['hello-everyone'];
      const output = matchMap(input, /(hello)(.+)$/, matches => {
        return ['goodbye', matches[2]];
      });

      expect(output).toStrictEqual(['goodbye', '-everyone']);
    });

    it('should not map non-matches', () => {
      const mapFn = jest.fn();

      const input = ['hello', 'world'];
      const output = matchMap(input, /any-text/, mapFn);

      expect(output).toStrictEqual(input);
      expect(mapFn).not.toHaveBeenCalled();
    });
  });
});
