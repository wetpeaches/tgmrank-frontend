module.exports = {
  parser: require('postcss-comment'),
  plugins: {
    'postcss-nested': true,
    'postcss-normalize': true,
    autoprefixer: {
      grid: true,
    },
    'postcss-reporter': true,
  },
};
