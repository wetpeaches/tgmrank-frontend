// ***********************************************
// This example commands.js shows you how to
// create various custom commands and overwrite
// existing commands.
//
// For more comprehensive examples of custom
// commands please read more here:
// https://on.cypress.io/custom-commands
// ***********************************************
//
//
// -- This is a parent command --
// Cypress.Commands.add("login", (email, password) => { ... })
//
//
// -- This is a child command --
// Cypress.Commands.add("drag", { prevSubject: 'element'}, (subject, options) => { ... })
//
//
// -- This is a dual command --
// Cypress.Commands.add("dismiss", { prevSubject: 'optional'}, (subject, options) => { ... })
//
//
// -- This will overwrite an existing command --
// Cypress.Commands.overwrite("visit", (originalFn, url, options) => { ... })

Cypress.Commands.add('login', (username, password) => {
  cy.get('#frmUsernameA').type(username);
  cy.get('#frmPasswordA').type(password);

  cy.get('[data-action="login"]').click();
});

Cypress.Commands.add('validatePath', path => {
  cy.url().should('eq', Cypress.config().baseUrl + path);
});

Cypress.Commands.add('stubCors', () => {
  cy.intercept('OPTIONS', '**/v1/**', {
    statusCode: 200,
    headers: {
      'access-control-allow-credentials': 'true',
      'access-control-allow-headers': 'content-type',
      'access-control-allow-methods': 'GET,OPTIONS,POST,PUT,PATCH,DELETE,HEAD',
      'access-control-allow-origin': Cypress.config().baseUrl,
      'access-control-max-age': '3600',
    },
  });
});

Cypress.Commands.add('stubLogin', () => {
  cy.intercept('POST', '**/v1/login', {
    statusCode: 200,
    headers: {
      'access-control-allow-credentials': 'true',
      'access-control-allow-headers': 'content-type',
      'access-control-allow-origin': Cypress.config().baseUrl,
    },
    fixture: 'login.success.json',
  });
});

Cypress.Commands.add('stubLoginVerify', () => {
  cy.intercept('GET', '**/v1/login/verify', {
    statusCode: 200,
    headers: {
      'access-control-allow-credentials': 'true',
      'access-control-allow-headers': 'content-type',
      'access-control-allow-origin': Cypress.config().baseUrl,
    },
    fixture: 'login.success.json',
  });
});

Cypress.Commands.add('stubLogout', () => {
  return cy.intercept('GET', '**/v1/logout', {
    statusCode: 200,
    body: {
      success: true,
    },
  });
});
