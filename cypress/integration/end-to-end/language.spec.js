function selectLanguage(language) {
  const languageMap = new Map();
  languageMap['en'] = 0;
  languageMap['fr'] = 1;
  languageMap['ja'] = 2;
  languageMap['de'] = 3;

  const languageIndex = languageMap[language.toLowerCase()];

  cy.get('#menu-button--menu--1 > .nav__icon').click('center', {
    animationDistanceThreshold: 2,
    waitForAnimations: true,
    scrollBehavior: 'nearest',
  });
  cy.get(`#option-${languageIndex}--menu--1`).click({
    animationDistanceThreshold: 2,
    waitForAnimations: true,
    scrollBehavior: 'nearest',
  });
}

function selectLanguageFooter(language) {
  cy.get(`.footer__languages .${language}`).click();
}

describe('Locale', () => {
  describe('Header', () => {
    it('should change language to french and back', () => {
      cy.stubCors();
      cy.intercept('GET', '**/v1/score/activity*', {
        statusCode: 200,
        body: [],
      });

      cy.visit('/');
      cy.get('.homepage__nav h2').should('have.text', 'Browse Leaderboards');
      cy.getCookie('locale').should('have.property', 'value', 'en');

      selectLanguage('fr');
      cy.get('.homepage__nav h2').should('have.text', 'Voir les classements');
      cy.getCookie('locale').should('have.property', 'value', 'fr');

      selectLanguage('en');
      cy.get('.homepage__nav h2').should('have.text', 'Browse Leaderboards');
      cy.getCookie('locale').should('have.property', 'value', 'en');
    });
  });

  describe('Footer', () => {
    it('should change language to german and back', () => {
      cy.stubCors();
      cy.intercept('GET', '**/v1/score/activity*', {
        statusCode: 200,
        body: [],
      });

      cy.visit('/');
      cy.get('.homepage__nav h2').should('have.text', 'Browse Leaderboards');
      cy.getCookie('locale').should('have.property', 'value', 'en');

      selectLanguageFooter('de');
      cy.get('.homepage__nav h2').should('have.text', 'Bestenlisten anschauen');
      cy.getCookie('locale').should('have.property', 'value', 'de');

      selectLanguageFooter('en');
      cy.get('.homepage__nav h2').should('have.text', 'Browse Leaderboards');
      cy.getCookie('locale').should('have.property', 'value', 'en');
    });
  });
});
